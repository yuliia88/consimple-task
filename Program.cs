﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TestTask
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GetResponse().Wait();
        }

        static async Task GetResponse()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://tester.consimple.pro");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage httpResponseMessage = await client.GetAsync("/");
                httpResponseMessage.EnsureSuccessStatusCode();

                string jsonResponse = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    Root deserializedData = JsonConvert.DeserializeObject<Root>(jsonResponse);

                    Console.WriteLine($"Products: \t\t Category:");
                    foreach (var product in deserializedData.Products)
                    {
                        foreach(var category in deserializedData.Categories)
                        {
                            if(product.CategoryId == category.Id)
                            {
                                Console.WriteLine($"{product.Name} \t\t {category.Name}");
                            }
                        }
                    }
                    Console.ReadLine();
                }
            }
        }
    }
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
    }

    public class Root
    {
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }
    }
}

